// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App.vue'
import axios from 'axios'
import Head from './components/Head.vue'
import ServiceHeader from './ServiceHeader'
import ServiceFooter from './ServiceFooter'
import Vuelidate from 'vuelidate'

import router from './router'
import store from './store'
 
Vue.config.productionTip = false

Vue.use(Vuelidate)
axios.defaults.baseURL = 'https://db-for-vuejs.firebaseio.com'

/* Register global components */
Vue.component('service-header', ServiceHeader)
Vue.component('service-footer', ServiceFooter)

// register global filter
Vue.filter('countChars', function (value){
	return value + ' (' + value.length + ')';
})
Vue.filter('firstCharCapital', function (value){
	return value.charAt(0).toUpperCase() + value.slice(1);
})

export const eventBus = new Vue;
export const servicesBus = new Vue;

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  // template: '<App/>',
  // components: { App }
  render: h => h(App)
})

new Vue({
	el: '#head',
	render: h => h(Head)
})
