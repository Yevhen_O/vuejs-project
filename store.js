import Vue from 'Vue'
import Vuex from 'vuex'
import axiosAuth from './axios_auth.js'
import axios from 'axios'

Vue.use(Vuex)

export default new Vuex.Store({
	state: {
		idToken: null,
		userId: null,
		counter: 0,
		users: []
	},
	mutations: {
		authUser (state, authData) {
			state.idToken = authData.idToken,
			state.userId = authData.userId
		},
		setUsers (state, users) {
			state.users = users
		}
	},
	actions: {
		signup ({commit, dispatch}, authData) {
			axiosAuth.post('/signupNewUser?key=AIzaSyBwVGuXEjdXsblXg3Fxl6CbbuEa4hXgpcE', {
				email: authData.email,
				password: authData.password,
				returnSecureToken: true
			})
			.then( res => {
				console.log(res);
				commit('authUser',{
					idToken: res.data.idToken,
					userId: res.data.localId
				})
				dispatch('storeUser', authData)
			})
			.catch( err => {
				console.log(err);
			})			
		},
		login ({commit}, authData) {
			axiosAuth.post('/verifyPassword?key=AIzaSyBwVGuXEjdXsblXg3Fxl6CbbuEa4hXgpcE', {
				email: authData.email,
				password: authData.password,
				returnSecureToken: true
			})
			.then( res => {
				console.log(res);
				commit('authUser',{
					idToken: res.data.idToken,
					userId: res.data.localId
				})				
			})
			.catch( err => {
				console.log(err);
			})			
		},
		storeUser ({commit, state}, userData) {
			if(!state.idToken){
				return
			}
			axios.post('/users.json?auth='+state.idToken, userData)
			.then( res => {
				console.log(res);
			})
			.catch( error => {
				console.log(error);
			});			
		},
		fetchUsers ({commit, state}) {
			if(!state.idToken){
				return
			}
			axios.get('/users.json?auth='+state.idToken)
                .then( res => {
                    const data = res.data;
                    const users = [];
                    for (let key in data) {
                        const user = data[key]
                        user.id = key
                        users.push(user)
                    }
                    console.log(res);
                    commit('setUsers', users)
                })
                .catch( error => console.log(error));			
		}
	},
	getters: {
   		getUsers (state) {
   			return state.users;
   		}
	}
})