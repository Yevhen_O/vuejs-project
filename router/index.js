import Vue from 'vue'
import Router from 'vue-router'

const User = resolve => {
	require.ensure(['../components/User.vue'], () => {
		resolve(require('../components/User.vue'))
	})
}
const Services = resolve => {
	require.ensure(['../ServiceStatus.vue'], () => {
		resolve(require('../ServiceStatus.vue'))
	})
}
const Dunamic = resolve => {
	require.ensure(['../components/Dunamic.vue'], () => {
		resolve(require('../components/Dunamic.vue'))
	})
}
const Messaging = resolve => {
	require.ensure(['../components/messaging/Messaging.vue'], () => {
		resolve(require('../components/messaging/Messaging.vue'))
	})
}
const Forms = resolve => {
	require.ensure(['../components/forms/Forms.vue'], () => {
		resolve(require('../components/forms/Forms.vue'))
	})
}
const Directives = resolve => {
	require.ensure(['../components/Directives.vue'], () => {
		resolve(require('../components/Directives.vue'))
	})
}
const FiltersMixins = resolve => {
	require.ensure(['../components/FiltersMixins.vue'], () => {
		resolve(require('../components/FiltersMixins.vue'))
	})
}
const Animations = resolve => {
	require.ensure(['../components/Animations.vue'], () => {
		resolve(require('../components/Animations.vue'))
	})
}
const MathQuiz = resolve => {
	require.ensure(['../components/mathquiz/MathQuiz.vue'], () => {
		resolve(require('../components/mathquiz/MathQuiz.vue'))
	})
}
const AxiosPage = resolve => {
	require.ensure(['../components/axiospage/AxiosPage.vue'], () => {
		resolve(require('../components/axiospage/AxiosPage.vue'))
	})
}
// import User from '../components/User.vue'
// import Services from '../ServiceStatus.vue'
// import Dunamic from '../components/Dunamic.vue'
// import Messaging from '../components/messaging/Messaging.vue'
// import Forms from '../components/forms/Forms.vue'
// import Directives from '../components/Directives.vue'
// import FiltersMixins from '../components/FiltersMixins.vue'
// import Animations from '../components/Animations.vue'
// import MathQuiz from '../components/mathquiz/MathQuiz.vue'
// import AxiosPage from '../components/axiospage/AxiosPage.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'User page',
      component: User
    },
    {
      path: '/services',
      name: 'Services page',
      component: Services
    },
    {
      path: '/dunamic',
      name: 'Dunamic page',
      component: Dunamic
    },
    {
      path: '/messaging',
      name: 'Messaging page',
      component: Messaging
    },
    {
      path: '/forms',
      name: 'Forms page',
      component: Forms
    },
    {
      path: '/directives',
      name: 'Directives page',
      component: Directives
    },
    {
      path: '/filters_mixins',
      name: 'Filters Mixins page',
      component: FiltersMixins
    },
    {
      path: '/animations',
      name: 'Animations page',
      component: Animations
    },
    {
      path: '/math_quiz',
      name: 'MathQuiz page',
      component: MathQuiz
    },
    {
      path: '/axios',
      name: 'Axios page',
      component: AxiosPage
    }
  ]
})
